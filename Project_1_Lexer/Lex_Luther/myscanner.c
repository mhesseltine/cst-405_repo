#include <stdio.h>
#include "header.h"

extern int yylex();
extern int yylineno;
extern char* yytext;

int main(void)
{
	int ntoken, vtoken;

	ntoken = yylex();
	while(ntoken)
	{
		//printf("%s\n", yytext);

		if(ntoken == 1)
			printf("Identifier: %s\n", yytext);
		else if(ntoken == 2)
			printf("Constant: %s\n", yytext);
		else if(ntoken == 3)
			printf("End Line: %s\n", yytext);
		else if(ntoken == 4)
			printf("CONTAINER_LEFTPAREN: %s\n", yytext);
		else if(ntoken == 5)
			printf("CONTAINER_RIGHTPAREN: %s\n", yytext);
		else if(ntoken == 6)
			printf("CONTAINER_OPENCURLYBRACE: %s\n", yytext);
		else if(ntoken == 7)
			printf("CONTAINER_CLOSECURLYBRACE: %s\n", yytext);
		else if(ntoken == 8)
			printf("CONTAINER_OPENSQUAREBRACE: %s\n", yytext);
		else if(ntoken == 9)
			printf("CONTAINER_CLOSESQUAREBRACE %s\n", yytext);
		else if(ntoken == 10)
			printf("KEYWORD_IF: %s\n", yytext);
		else if(ntoken == 11)
			printf("KEYWORD_AND: %s\n", yytext);
		else if(ntoken == 12)
			printf("KEYWORD_OR: %s\n", yytext);
		else if(ntoken == 13)
			printf("KEYWORD_ELSE: %s\n", yytext);
		else if(ntoken == 14)
			printf("KEYWORD_THEN: %s\n", yytext);
		else if(ntoken == 15)
			printf("KEYWORD_ELIF: %s\n", yytext);
		else if(ntoken == 16)
			printf("KEYWORD_SWITCH: %s\n", yytext);
		else if(ntoken == 17)
			printf("KEYWORD_CASE: %s\n", yytext);
		else if(ntoken == 18)
			printf("KEYWORD_BREAK: %s\n", yytext);
		else if(ntoken == 19)
			printf("KEYWORD_DEFAULT: %s\n", yytext);
		else if(ntoken == 20)
			printf("KEYWORD_CONTINUE: %s\n", yytext);
		else if(ntoken == 21)
			printf("KEYWORD_RETURN: %s\n", yytext);
		else if(ntoken == 22)
			printf("KEYWORD_WRITE: %s\n", yytext);
		else if(ntoken == 23)
			printf("KEYWORD_WRITELN: %s\n", yytext);
		else if(ntoken == 24)
			printf("KEYWORD_PUBLIC: %s\n", yytext);
		else if(ntoken == 25)
			printf("KEYWORD_PRIVATE: %s\n", yytext);
		else if(ntoken == 26)
			printf("OPERATOR_ADD: %s\n", yytext);
		else if(ntoken == 27)
			printf("OPERATOR_SUBTRACT: %s\n", yytext);
		else if(ntoken == 28)
			printf("OPERATOR_MULTIPLY: %s\n", yytext);
		else if(ntoken == 29)
			printf("OPERATOR_DIVIDE: %s\n", yytext);
		else if(ntoken == 30)
			printf("OPERATOR_MODDIVIDE: %s\n", yytext);
		else if(ntoken == 31)
			printf("OPERATOR_DOT: %s\n", yytext);
		else if(ntoken == 32)
			printf("OPERATOR_EXPONENT: %s\n", yytext);
		else if(ntoken == 33)
			printf("OPERATOR_SEPARATOR: %s\n", yytext);
		else if(ntoken == 34)
			printf("OPERATOR_ASSIGNMENT: %s\n", yytext);
		else if(ntoken == 35)
			printf("COMPARISON_LESSTHAN: %s\n", yytext);
		else if(ntoken == 36)
			printf("COMPARISON_GREATERTHAN: %s\n", yytext);
		else if(ntoken == 37)
			printf("COMPARISON_LESSTHANOREQUALTO %s\n", yytext);
		else if(ntoken == 38)
			printf("COMPARISON_GREATERTHANOREQUALTO %s\n", yytext);
		else if(ntoken == 39)
			printf("COMARPSION_EQUIVALENCY: %s\n", yytext);
		else if(ntoken == 40)
			printf("COMPARISON_NOTEQUIVALENT: %s\n", yytext);
		else if(ntoken == 41)
			printf("COMPARISON_COMPLEMENT %s\n", yytext);
		else if(ntoken == 42)
			printf("COMMENT: %s\n", yytext);

		ntoken = yylex();
	}
	return 0;
}
