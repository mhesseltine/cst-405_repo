# Project 2: Syntax Analyzer (Parser)


- Download folder containing the project

- Open terminal in Linux

- Navigate to directory contining the project

- Compile the project using the "make" command

- Generate output by entering ./ParsleyTheParser.out input

- The three input files you can test are input, input2, and input3
