/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_LANGUAGE_TAB_H_INCLUDED
# define YY_YY_LANGUAGE_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    KEYWORD_IF = 258,
    KEYWORD_AND = 259,
    KEYWORD_OR = 260,
    KEYWORD_ELSE = 261,
    KEYWORD_THEN = 262,
    KEYWORD_ELIF = 263,
    KEYWORD_SWITCH = 264,
    KEYWORD_CASE = 265,
    KEYWORD_BREAK = 266,
    KEYWORD_DEFAULT = 267,
    KEYWORD_CONTINUE = 268,
    KEYWORD_RETURN = 269,
    KEYWORD_PRIVATE = 270,
    KEYWORD_PUBLIC = 271,
    KEYWORD_WRITE = 272,
    KEYWORD_WRITELN = 273,
    KEYWORD_WHILE = 274,
    CONTAINER_LEFTPAREN = 275,
    CONTAINER_RIGHTPAREN = 276,
    CONTAINER_OPENCURLYBRACE = 277,
    CONTAINER_CLOSECURLYBRACE = 278,
    CONTAINER_OPENSQUAREBRACE = 279,
    CONTAINER_CLOSESQUAREBRACE = 280,
    COMPARISON_LESSTHAN = 281,
    COMPARISON_GREATERTHAN = 282,
    COMPARISON_LESSTHANOREQUALTO = 283,
    COMPARISON_GREATERTHANOREQUALTO = 284,
    COMPARISON_EQUIVALENCY = 285,
    COMPARISON_NOTEQUIVALENT = 286,
    COMPARISON_COMPLEMENT = 287,
    OPERATOR_ADD = 288,
    OPERATOR_SUBTRACT = 289,
    OPERATOR_DIVIDE = 290,
    OPERATOR_MULTIPLY = 291,
    OPERATOR_MODDIVIDE = 292,
    OPERATOR_DOT = 293,
    OPERATOR_EXPONENT = 294,
    OPERATOR_SEPARATOR = 295,
    OPERATOR_ASSIGNMENT = 296,
    TYPE_INT = 297,
    TYPE_STRING = 298,
    TYPE_DOUBLE = 299,
    TYPE_CHAR = 300,
    TYPE_FLOAT = 301,
    TYPE_BOOLEAN = 302,
    TYPE_LONGINT = 303,
    TYPE_VAR = 304,
    TYPE_VOID = 305,
    IDENTIFIER = 306,
    NUM = 307,
    DOUBLE = 308
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 15 "language.y" /* yacc.c:1909  */
int value; struct generateTree *operation;

#line 111 "language.tab.h" /* yacc.c:1909  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_LANGUAGE_TAB_H_INCLUDED  */
