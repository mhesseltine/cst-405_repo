%{
#include "language.tab.h"
%}

%%

"("			{return CONTAINER_LEFTPAREN;}
")"			{return CONTAINER_RIGHTPAREN;}
"{"			{return CONTAINER_OPENCURLYBRACE;}
"}"			{return CONTAINER_CLOSECURLYBRACE;}
"["			{return CONTAINER_OPENSQUAREBRACE;}
"]"			{return CONTAINER_CLOSESQUAREBRACE;}

"if"			{return KEYWORD_IF;}
"and"			{return KEYWORD_AND;}
"or"			{return KEYWORD_OR;}
"else"			{return KEYWORD_ELSE;}
"then"			{return KEYWORD_THEN;}
"elif"			{return KEYWORD_ELIF;}
"switch"		{return KEYWORD_SWITCH;}
"case"			{return KEYWORD_CASE;}
"break"			{return KEYWORD_BREAK;}
"default"		{return KEYWORD_DEFAULT;}
"continue"		{return KEYWORD_CONTINUE;}
"while"			{return KEYWORD_WHILE;}
"return"		{return KEYWORD_RETURN;}
"write"			{return KEYWORD_WRITE;}
"writeln"		{return KEYWORD_WRITELN;}
"private"		{return KEYWORD_PRIVATE;}
"public"		{return KEYWORD_PUBLIC;}


"<"			{return COMPARISON_LESSTHAN;}
">"			{return COMPARISON_GREATERTHAN;}
"<="			{return COMPARISON_LESSTHANOREQUALTO;}
">="			{return COMPARISON_GREATERTHANOREQUALTO;}
"=="			{return COMPARISON_EQUIVALENCY;}
"!="			{return COMPARISON_NOTEQUIVALENT;}
"!"			{return COMPARISON_COMPLEMENT;}

"+"			{return OPERATOR_ADD;}
"\-"			{return OPERATOR_SUBTRACT;}
"/"			{return OPERATOR_DIVIDE;}
"*"			{return OPERATOR_MULTIPLY;}
"%"			{return OPERATOR_MODDIVIDE;}			
"."			{return OPERATOR_DOT;}
"**"			{return OPERATOR_EXPONENT;}
","			{return OPERATOR_SEPARATOR;}
"="			{return OPERATOR_ASSIGNMENT;}			

"int"			{return TYPE_INT;}
"string"		{return TYPE_STRING;}
"double"		{return TYPE_DOUBLE;}
"float"			{return TYPE_FLOAT;}
"boolean"		{return TYPE_BOOLEAN;}
"longint"		{return TYPE_LONGINT;}
"var"			{return TYPE_VAR;}
"void"			{return TYPE_VOID;}
"char"			{return TYPE_CHAR;}

[a-zA-Z][_a-zA-Z0-9|']* {return IDENTIFIER;}
\"(\\.|[^"\\])*\"	{return IDENTIFIER;}
[0-9][0-9]*		{return NUM;}
[0-9]*"."[0-9][0-9]*	{return DOUBLE;}

[ \t\n]                ;
[-+*/=;:]              {return yytext[0];}
.                      {ECHO; printf("unexpected character");}

%%

int yywrap (void) {return 1;}

