%{
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <iostream>
#define YYDEBUG 1
void yyerror (std::string s);
extern int yylex();
extern int yyparse();
extern char* yytext;
extern FILE *yyin;
%}

%union {int value; struct generateTree *operation;}        /* Yacc definitions */

%start program

%token <value> KEYWORD_IF
%token <value> KEYWORD_AND
%token <value> KEYWORD_OR
%token <value> KEYWORD_ELSE
%token <value> KEYWORD_THEN
%token <value> KEYWORD_ELIF
%token <value> KEYWORD_SWITCH
%token <value> KEYWORD_CASE
%token <value> KEYWORD_BREAK
%token <value> KEYWORD_DEFAULT
%token <value> KEYWORD_CONTINUE
%token <value> KEYWORD_RETURN
%token <value> KEYWORD_PRIVATE
%token <value> KEYWORD_PUBLIC
%token <value> KEYWORD_WRITE
%token <value> KEYWORD_WRITELN
%token <value> KEYWORD_WHILE

%token <value> CONTAINER_LEFTPAREN
%token <value> CONTAINER_RIGHTPAREN
%token <value> CONTAINER_OPENCURLYBRACE
%token <value> CONTAINER_CLOSECURLYBRACE
%token <value> CONTAINER_OPENSQUAREBRACE
%token <value> CONTAINER_CLOSESQUAREBRACE

%token <value> COMPARISON_LESSTHAN
%token <value> COMPARISON_GREATERTHAN
%token <value> COMPARISON_LESSTHANOREQUALTO
%token <value> COMPARISON_GREATERTHANOREQUALTO
%token <value> COMPARISON_EQUIVALENCY
%token <value> COMPARISON_NOTEQUIVALENT
%token <value> COMPARISON_COMPLEMENT

%token <value> OPERATOR_ADD
%token <value> OPERATOR_SUBTRACT
%token <value> OPERATOR_DIVIDE
%token <value> OPERATOR_MULTIPLY
%token <value> OPERATOR_MODDIVIDE
%token <value> OPERATOR_DOT
%token <value> OPERATOR_EXPONENT
%token <value> OPERATOR_SEPARATOR
%token <value> OPERATOR_ASSIGNMENT

%token <value> TYPE_INT
%token <value> TYPE_STRING
%token <value> TYPE_DOUBLE
%token <value> TYPE_CHAR
%token <value> TYPE_FLOAT
%token <value> TYPE_BOOLEAN
%token <value> TYPE_LONGINT
%token <value> TYPE_VAR
%token <value> TYPE_VOID
%token <value> IDENTIFIER

%token <value> NUM
%token <value> DOUBLE

%type <operation> ParamDecl
%type <operation> ParamDeclList
%type <operation> ParamDeclListTail
%type <operation> StmtList
%type <operation> Stmt
%type <operation> Block
%type <operation> expression
%type <operation> UnaryOp
%type <operation> ExprList
%type <operation> ExprListTail
%type <operation> TYPE
%type <operation> BinaryOp
%type <operation> Primary
%type <operation> NumberType

%%

/* descriptions of expected inputs corresponding actions*/

program			: StmtList
			;


ParamDeclList		: /*empty*/		{std::cout<<"Recognized empty parameters\n";}
			| ParamDeclListTail	{std::cout<<"Recognized ParamDeclListTail\n";}
			;

ParamDeclListTail	: ParamDecl						{std::cout<<"Recognized a ParamDecl\n";}
			| ParamDecl OPERATOR_SEPARATOR ParamDeclListTail	{std::cout<<"Recognized a ParamList\n";}
			;

ParamDecl		: TYPE IDENTIFIER							{std::cout<<"Recognized ParamDecl\n";}
			| TYPE IDENTIFIER CONTAINER_OPENSQUAREBRACE CONTAINER_CLOSESQUAREBRACE	{std::cout<<"Recognized Brackets ParamDecl";}
			;


Block			: CONTAINER_OPENCURLYBRACE StmtList CONTAINER_CLOSECURLYBRACE	{std::cout<<"Recognized the end a Block\n";}
			;

StmtList		: Stmt		{std::cout<<"Recognized a statement\n";} 
			| Stmt StmtList	{std::cout<<"Recognized a statement recursion\n";}
			;

Stmt			: /*empty*/										;
			| ';'											{std::cout<<"Recognized a semi-colon ending statement\n";}
			| KEYWORD_IF CONTAINER_LEFTPAREN expression CONTAINER_RIGHTPAREN Stmt			{std::cout<<"Recognized If statement\n";}
			| KEYWORD_THEN expression								{std::cout<<"Recognized Then statement\n";}
			| KEYWORD_ELIF CONTAINER_LEFTPAREN expression CONTAINER_RIGHTPAREN Stmt			{std::cout<<"Recognized Elif statment\n";}
			| KEYWORD_ELSE Stmt									{std::cout<<"Recognized Else statement\n";}
			| KEYWORD_SWITCH CONTAINER_LEFTPAREN IDENTIFIER CONTAINER_RIGHTPAREN Block		{std::cout<<"Recognized Switch statement\n";}
			| KEYWORD_CASE NUM ':' Stmt KEYWORD_BREAK ';'						{std::cout<<"Recognized Case statement\n";}
			| KEYWORD_CASE IDENTIFIER ':' Stmt KEYWORD_BREAK ';'					{std::cout<<"Recognized a Case statement\n";}
			| KEYWORD_DEFAULT ':' Stmt KEYWORD_BREAK ';'						{std::cout<<"Recognized Default statment\n";}
			| KEYWORD_DEFAULT ';'									{std::cout<<"Recognized Default statement\n";}
			| KEYWORD_CONTINUE									{std::cout<<"Recognize Continue statement\n";}
			| KEYWORD_RETURN ';'									{std::cout<<"Recognized a return statement\n";}
			| KEYWORD_RETURN NumberType ';'								{std::cout<<"Recognized Return statement\n";}
			| KEYWORD_RETURN IDENTIFIER ';'								{std::cout<<"Recognized Return statement\n";}
			| KEYWORD_RETURN CONTAINER_LEFTPAREN NumberType CONTAINER_RIGHTPAREN ';'		{std::cout<<"Recognized Return statement\n";}
			| KEYWORD_RETURN CONTAINER_LEFTPAREN IDENTIFIER CONTAINER_RIGHTPAREN ';'		{std::cout<<"Recognized Return statement\n";}
			| KEYWORD_WRITE CONTAINER_LEFTPAREN IDENTIFIER CONTAINER_RIGHTPAREN ';'			{std::cout<<"Recognized Write statement\n";}
			| KEYWORD_WRITELN CONTAINER_LEFTPAREN IDENTIFIER CONTAINER_RIGHTPAREN ';'		{std::cout<<"Recognized WriteLn statement\n";}
			| KEYWORD_PUBLIC TYPE IDENTIFIER CONTAINER_LEFTPAREN Stmt CONTAINER_RIGHTPAREN		{std::cout<<"Recognized Public statement\n";}
			| KEYWORD_PRIVATE TYPE IDENTIFIER CONTAINER_LEFTPAREN Stmt CONTAINER_RIGHTPAREN		{std::cout<<"Recognized Private statement\n";}
			| KEYWORD_WHILE CONTAINER_LEFTPAREN expression CONTAINER_RIGHTPAREN			{std::cout<<"Recognized While statment\n";}
			| IDENTIFIER OPERATOR_ASSIGNMENT expression ';'						{std::cout<<"Recognized an re-assignment statement\n";}
			| TYPE IDENTIFIER ';'									{std::cout<<"Recognized an identifier\n";}
			| TYPE IDENTIFIER OPERATOR_ASSIGNMENT expression ';'					{std::cout<<"Recognized an assignment statement\n";}
			| TYPE IDENTIFIER CONTAINER_OPENSQUAREBRACE NumberType CONTAINER_CLOSESQUAREBRACE	{std::cout<<"Recognized an array declaration\n";}
			| TYPE IDENTIFIER CONTAINER_OPENSQUAREBRACE Stmt CONTAINER_CLOSESQUAREBRACE ';'		{std::cout<<"Recognized a multi-dimensional array declaration\n";}
			| TYPE IDENTIFIER CONTAINER_LEFTPAREN ParamDeclList CONTAINER_RIGHTPAREN Block		{std::cout<<"Recognizes a declaration\n";}
			| Block
			;

expression		: Primary													{std::cout<<"Recognized a Primary Expression\n";}
			| NumberType													;
			| UnaryOp expression												{std::cout<<"Recognized Unary expression\n";}
			| expression BinaryOp expression										{std::cout<<"Recognized Binary expression\n";}
			| expression KEYWORD_AND expression										{std::cout<<"Recognized And keyword\n";}
			| expression KEYWORD_OR expression										{std::cout<<"Recognized Or keyword";}
			| IDENTIFIER CONTAINER_LEFTPAREN ExprList CONTAINER_RIGHTPAREN							{std::cout<<"Recognized an Expression list\n";}
			| IDENTIFIER OPERATOR_ASSIGNMENT expression									{std::cout<<"Recognized Declaration Expression\n";}
			| IDENTIFIER CONTAINER_OPENSQUAREBRACE expression CONTAINER_CLOSESQUAREBRACE OPERATOR_ASSIGNMENT expression	{std::cout<<"Recognized Bracketed Expression\n";}
			;

Primary			: IDENTIFIER									{std::cout<<"Recognized an Identifier\n";}
			| TYPE										{std::cout<<"Recognized a type\n";}
			| CONTAINER_LEFTPAREN expression CONTAINER_RIGHTPAREN				{std::cout<<"Recognized Parenthesis Expression\n";}
			| IDENTIFIER CONTAINER_LEFTPAREN expression CONTAINER_RIGHTPAREN		{std::cout<<"Recognized a Parenthesis Expression\n";}
			| IDENTIFIER CONTAINER_OPENSQUAREBRACE expression CONTAINER_CLOSESQUAREBRACE	{std::cout<<"Recognized a Square Bracket expression\n";}
			;

ExprList		: /*empty*/	{std::cout<<"Recognized Empty ExprList\n";}
			| ExprListTail	{std::cout<<"Recognized ExprListTail\n";}
			;

ExprListTail		: expression					{std::cout<<"Recognized Expression\n";}
			| expression OPERATOR_SEPARATOR ExprListTail	{std::cout<<"Recognized Expression Recursion\n";}
			;

UnaryOp			: OPERATOR_SUBTRACT		{std::cout<<"Recognized subtraction operator\n";}
			| COMPARISON_NOTEQUIVALENT	{std::cout<<"Recognized non-equivalency comparison\n";}
			;

BinaryOp		: OPERATOR_ADD				{std::cout<<"Recognized Addition operator\n";}
			| OPERATOR_SUBTRACT			{std::cout<<"Recognized Subtraction operator\n";}
			| OPERATOR_DIVIDE			{std::cout<<"Recognized Division operator \n";}
			| OPERATOR_MULTIPLY			{std::cout<<"Recognized Multiplication operator\n";}
			| OPERATOR_MODDIVIDE			{std::cout<<"Recognized Modular divison operator\n";}
			| OPERATOR_DOT				{std::cout<<"Recognized Dot operator\n";}
			| OPERATOR_EXPONENT			{std::cout<<"Recognized Exponent operator\n";}
			| OPERATOR_ASSIGNMENT			{std::cout<<"Recognized Assignment operator\n";}
			| COMPARISON_EQUIVALENCY		{std::cout<<"Recognized Equivalency evaluation\n";}
			| COMPARISON_LESSTHAN			{std::cout<<"Recognized Less Than evaluation\n";}
			| COMPARISON_GREATERTHAN		{std::cout<<"Recognized Greater Than evaluation\n";}
			| COMPARISON_LESSTHANOREQUALTO		{std::cout<<"Recognized Less Than or Equal To evaluation\n";}
			| COMPARISON_GREATERTHANOREQUALTO	{std::cout<<"Recognized Greater Than or Equal to evaluation\n";}
			| COMPARISON_NOTEQUIVALENT		{std::cout<<"Recognized Not Equivalent evaluation\n";}
			;

TYPE			: TYPE_INT	{std::cout<<"Recognized Int type\n";}
			| TYPE_STRING	{std::cout<<"Recognized String type\n";}
			| TYPE_DOUBLE	{std::cout<<"Recognized Double type\n";}
			| TYPE_CHAR	{std::cout<<"Recognized Char type\n";}
			| TYPE_FLOAT	{std::cout<<"Recognized Float type\n";}
			| TYPE_BOOLEAN	{std::cout<<"Recognized Boolean type\n";}
			| TYPE_LONGINT	{std::cout<<"Recognized LongInt type\n";}
			| TYPE_VAR	{std::cout<<"Recognized Var type\n";}
			| TYPE_VOID	{std::cout<<"Recognized Void type\n";}
			;

NumberType		: NUM		{std::cout<<"Recognized a number\n";}
			| DOUBLE	{std::cout<<"Recognized a double\n";}
			;

%%

/* C++ code */

int main (int argc, char** argv)
{
	if (argc == 1)
	{
		std::cout<<"no source\n";
		return 0;
	}
	yyin = fopen(argv[1], "r");
	if(!yyin)
	{
		std::cout<<"could not open file\n";
		perror(argv[1]);
		return(1);
	}
	yyparse();
	return 0;
}

void yyerror (std::string error) 
{
	std::cout<< "Error: " << error << "\n";
}
