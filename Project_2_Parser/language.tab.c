/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 1 "language.y" /* yacc.c:339  */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <iostream>
#define YYDEBUG 1
void yyerror (std::string s);
extern int yylex();
extern int yyparse();
extern char* yytext;
extern FILE *yyin;

#line 80 "language.tab.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "language.tab.h".  */
#ifndef YY_YY_LANGUAGE_TAB_H_INCLUDED
# define YY_YY_LANGUAGE_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    KEYWORD_IF = 258,
    KEYWORD_AND = 259,
    KEYWORD_OR = 260,
    KEYWORD_ELSE = 261,
    KEYWORD_THEN = 262,
    KEYWORD_ELIF = 263,
    KEYWORD_SWITCH = 264,
    KEYWORD_CASE = 265,
    KEYWORD_BREAK = 266,
    KEYWORD_DEFAULT = 267,
    KEYWORD_CONTINUE = 268,
    KEYWORD_RETURN = 269,
    KEYWORD_PRIVATE = 270,
    KEYWORD_PUBLIC = 271,
    KEYWORD_WRITE = 272,
    KEYWORD_WRITELN = 273,
    KEYWORD_WHILE = 274,
    CONTAINER_LEFTPAREN = 275,
    CONTAINER_RIGHTPAREN = 276,
    CONTAINER_OPENCURLYBRACE = 277,
    CONTAINER_CLOSECURLYBRACE = 278,
    CONTAINER_OPENSQUAREBRACE = 279,
    CONTAINER_CLOSESQUAREBRACE = 280,
    COMPARISON_LESSTHAN = 281,
    COMPARISON_GREATERTHAN = 282,
    COMPARISON_LESSTHANOREQUALTO = 283,
    COMPARISON_GREATERTHANOREQUALTO = 284,
    COMPARISON_EQUIVALENCY = 285,
    COMPARISON_NOTEQUIVALENT = 286,
    COMPARISON_COMPLEMENT = 287,
    OPERATOR_ADD = 288,
    OPERATOR_SUBTRACT = 289,
    OPERATOR_DIVIDE = 290,
    OPERATOR_MULTIPLY = 291,
    OPERATOR_MODDIVIDE = 292,
    OPERATOR_DOT = 293,
    OPERATOR_EXPONENT = 294,
    OPERATOR_SEPARATOR = 295,
    OPERATOR_ASSIGNMENT = 296,
    TYPE_INT = 297,
    TYPE_STRING = 298,
    TYPE_DOUBLE = 299,
    TYPE_CHAR = 300,
    TYPE_FLOAT = 301,
    TYPE_BOOLEAN = 302,
    TYPE_LONGINT = 303,
    TYPE_VAR = 304,
    TYPE_VOID = 305,
    IDENTIFIER = 306,
    NUM = 307,
    DOUBLE = 308
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 15 "language.y" /* yacc.c:355  */
int value; struct generateTree *operation;

#line 177 "language.tab.c" /* yacc.c:355  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_LANGUAGE_TAB_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 194 "language.tab.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  62
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   446

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  56
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  16
/* YYNRULES -- Number of rules.  */
#define YYNRULES  85
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  173

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   308

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    55,    54,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint8 yyrline[] =
{
       0,    95,    95,    99,   100,   103,   104,   107,   108,   112,
     115,   116,   119,   120,   121,   122,   123,   124,   125,   126,
     127,   128,   129,   130,   131,   132,   133,   134,   135,   136,
     137,   138,   139,   140,   141,   142,   143,   144,   145,   146,
     147,   150,   151,   152,   153,   154,   155,   156,   157,   158,
     161,   162,   163,   164,   165,   168,   169,   172,   173,   176,
     177,   180,   181,   182,   183,   184,   185,   186,   187,   188,
     189,   190,   191,   192,   193,   196,   197,   198,   199,   200,
     201,   202,   203,   204,   207,   208
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "KEYWORD_IF", "KEYWORD_AND",
  "KEYWORD_OR", "KEYWORD_ELSE", "KEYWORD_THEN", "KEYWORD_ELIF",
  "KEYWORD_SWITCH", "KEYWORD_CASE", "KEYWORD_BREAK", "KEYWORD_DEFAULT",
  "KEYWORD_CONTINUE", "KEYWORD_RETURN", "KEYWORD_PRIVATE",
  "KEYWORD_PUBLIC", "KEYWORD_WRITE", "KEYWORD_WRITELN", "KEYWORD_WHILE",
  "CONTAINER_LEFTPAREN", "CONTAINER_RIGHTPAREN",
  "CONTAINER_OPENCURLYBRACE", "CONTAINER_CLOSECURLYBRACE",
  "CONTAINER_OPENSQUAREBRACE", "CONTAINER_CLOSESQUAREBRACE",
  "COMPARISON_LESSTHAN", "COMPARISON_GREATERTHAN",
  "COMPARISON_LESSTHANOREQUALTO", "COMPARISON_GREATERTHANOREQUALTO",
  "COMPARISON_EQUIVALENCY", "COMPARISON_NOTEQUIVALENT",
  "COMPARISON_COMPLEMENT", "OPERATOR_ADD", "OPERATOR_SUBTRACT",
  "OPERATOR_DIVIDE", "OPERATOR_MULTIPLY", "OPERATOR_MODDIVIDE",
  "OPERATOR_DOT", "OPERATOR_EXPONENT", "OPERATOR_SEPARATOR",
  "OPERATOR_ASSIGNMENT", "TYPE_INT", "TYPE_STRING", "TYPE_DOUBLE",
  "TYPE_CHAR", "TYPE_FLOAT", "TYPE_BOOLEAN", "TYPE_LONGINT", "TYPE_VAR",
  "TYPE_VOID", "IDENTIFIER", "NUM", "DOUBLE", "';'", "':'", "$accept",
  "program", "ParamDeclList", "ParamDeclListTail", "ParamDecl", "Block",
  "StmtList", "Stmt", "expression", "Primary", "ExprList", "ExprListTail",
  "UnaryOp", "BinaryOp", "TYPE", "NumberType", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,    59,    58
};
# endif

#define YYPACT_NINF -119

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-119)))

#define YYTABLE_NINF -1

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     181,   -12,   181,   393,    -8,    -4,   -30,   -20,  -119,    28,
     114,   114,     4,     9,    12,   181,  -119,  -119,  -119,  -119,
    -119,  -119,  -119,  -119,  -119,   -16,  -119,    46,  -119,  -119,
     181,     1,   393,  -119,   393,  -119,  -119,   -15,  -119,  -119,
     381,  -119,   393,  -119,  -119,   393,     2,    -1,    10,  -119,
     181,   -33,    -7,  -119,    -5,    22,    24,    25,    26,   393,
      60,   393,  -119,  -119,     3,   286,   305,   393,   393,   393,
     393,   393,  -119,  -119,  -119,  -119,  -119,  -119,  -119,  -119,
    -119,  -119,  -119,  -119,  -119,  -119,   393,   381,   324,    30,
     181,   181,    45,    63,    64,  -119,  -119,    66,    68,    71,
      72,   343,  -119,    33,   114,   129,   393,  -119,   181,  -119,
     248,    73,  -119,   267,   381,   381,   381,   381,   181,    67,
      86,    87,    47,    48,    49,   181,   181,    52,    53,  -119,
    -119,    79,  -119,    69,    65,    89,    90,   210,  -119,  -119,
     393,  -119,    70,  -119,  -119,    74,    75,  -119,  -119,  -119,
      96,    98,  -119,  -119,    67,   114,    97,    95,  -119,  -119,
     362,  -119,   393,  -119,  -119,  -119,  -119,  -119,  -119,    99,
    -119,   381,  -119
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
      12,     0,    12,     0,     0,     0,     0,     0,    23,     0,
       0,     0,     0,     0,     0,    12,    75,    76,    77,    78,
      79,    80,    81,    82,    83,     0,    13,     0,    40,     2,
      10,     0,     0,    17,     0,    60,    59,    50,    84,    85,
      15,    41,     0,    51,    42,     0,     0,     0,     0,    22,
      12,     0,     0,    24,     0,     0,     0,     0,     0,     0,
       0,     0,     1,    11,     0,     0,     0,    55,     0,     0,
       0,     0,    70,    71,    72,    73,    69,    74,    61,    62,
      63,    64,    65,    66,    67,    68,     0,    43,     0,     0,
      12,    12,     0,     0,     0,    26,    25,     0,     0,     0,
       0,     0,     9,     0,     3,    12,     0,    35,    12,    52,
       0,     0,    56,     0,    48,    45,    46,    44,    12,     0,
       0,     0,     0,     0,     0,    12,    12,     0,     0,    33,
      34,     0,     4,     5,     0,     0,     0,     0,    14,    53,
       0,    47,    54,    16,    18,     0,     0,    21,    28,    27,
       0,     0,    29,    30,     0,     0,     7,     0,    37,    36,
      57,    58,     0,    20,    19,    32,    31,    39,     6,     0,
      38,    49,     8
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
    -119,  -119,  -119,   -35,  -119,  -118,    -2,     5,   -28,  -119,
    -119,   -18,  -119,  -119,     0,    -6
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,    27,   131,   132,   133,    28,    29,    30,    40,    41,
     111,   112,    42,    86,    43,    44
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_uint8 yytable[] =
{
      31,   144,    31,    54,    65,    67,    66,    33,    32,    68,
      55,    56,    45,    60,    87,    31,    46,    88,    93,    38,
      39,    47,    48,   104,    57,    61,    69,   105,    63,    58,
      31,   101,    59,   103,    49,    50,   167,    70,    71,   110,
     113,   114,   115,   116,   106,    94,    62,    95,    51,    96,
      31,   119,    64,    89,    90,    92,   122,   107,   117,    72,
      73,    74,    75,    76,    77,    91,    78,    79,    80,    81,
      82,    83,    84,    97,    85,    98,    99,   100,   137,    52,
      38,    39,    53,   102,   123,   124,   125,   130,   126,    15,
      31,    31,   127,   128,   141,   120,   121,   145,   146,   136,
     154,   147,   148,   149,   134,    31,   152,   153,    31,   155,
     135,   162,   160,   138,   157,   158,   156,   165,    31,   166,
     168,   169,   161,   143,   172,    31,    31,     0,   163,   164,
     150,   151,     1,     0,   171,     2,     3,     4,     5,     6,
       0,     7,     8,     9,    10,    11,    12,    13,    14,   170,
       0,    15,     0,     0,     0,   134,    16,    17,    18,    19,
      20,    21,    22,    23,    24,     0,     0,     0,     0,     0,
       0,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    38,    39,    26,     1,     0,     0,     2,     3,     4,
       5,     6,     0,     7,     8,     9,    10,    11,    12,    13,
      14,     0,     0,    15,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    70,    71,     0,     0,     0,     0,
       0,     0,     0,    16,    17,    18,    19,    20,    21,    22,
      23,    24,    25,     0,     0,    26,    72,    73,    74,    75,
      76,    77,     0,    78,    79,    80,    81,    82,    83,    84,
       0,    85,    70,    71,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   159,     0,     0,     0,     0,   139,
       0,    70,    71,     0,    72,    73,    74,    75,    76,    77,
       0,    78,    79,    80,    81,    82,    83,    84,   140,    85,
      70,    71,   142,    72,    73,    74,    75,    76,    77,     0,
      78,    79,    80,    81,    82,    83,    84,   108,    85,    70,
      71,     0,    72,    73,    74,    75,    76,    77,     0,    78,
      79,    80,    81,    82,    83,    84,   109,    85,    70,    71,
       0,    72,    73,    74,    75,    76,    77,     0,    78,    79,
      80,    81,    82,    83,    84,   118,    85,    70,    71,     0,
      72,    73,    74,    75,    76,    77,     0,    78,    79,    80,
      81,    82,    83,    84,   129,    85,    70,    71,     0,    72,
      73,    74,    75,    76,    77,     0,    78,    79,    80,    81,
      82,    83,    84,     0,    85,    70,    71,     0,    72,    73,
      74,    75,    76,    77,     0,    78,    79,    80,    81,    82,
      83,    84,   140,    85,     0,     0,     0,    72,    73,    74,
      75,    76,    77,    34,    78,    79,    80,    81,    82,    83,
      84,     0,    85,     0,    35,     0,     0,    36,     0,     0,
       0,     0,     0,     0,     0,    16,    17,    18,    19,    20,
      21,    22,    23,    24,    37,    38,    39
};

static const yytype_int16 yycheck[] =
{
       0,   119,     2,     9,    32,    20,    34,     2,    20,    24,
      10,    11,    20,    15,    42,    15,    20,    45,    51,    52,
      53,    51,    52,    20,    20,    41,    41,    24,    30,    20,
      30,    59,    20,    61,    54,    55,   154,     4,     5,    67,
      68,    69,    70,    71,    41,    51,     0,    54,    20,    54,
      50,    21,    51,    51,    55,    50,    11,    54,    86,    26,
      27,    28,    29,    30,    31,    55,    33,    34,    35,    36,
      37,    38,    39,    51,    41,    51,    51,    51,   106,    51,
      52,    53,    54,    23,    21,    21,    20,    54,    20,    22,
      90,    91,    21,    21,    21,    90,    91,    11,    11,   105,
      21,    54,    54,    54,   104,   105,    54,    54,   108,    40,
     105,    41,   140,   108,    25,    25,    51,    21,   118,    21,
     155,    24,   140,   118,    25,   125,   126,    -1,    54,    54,
     125,   126,     3,    -1,   162,     6,     7,     8,     9,    10,
      -1,    12,    13,    14,    15,    16,    17,    18,    19,    54,
      -1,    22,    -1,    -1,    -1,   155,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    -1,    -1,    -1,    -1,    -1,
      -1,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,     3,    -1,    -1,     6,     7,     8,
       9,    10,    -1,    12,    13,    14,    15,    16,    17,    18,
      19,    -1,    -1,    22,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,     4,     5,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    -1,    -1,    54,    26,    27,    28,    29,
      30,    31,    -1,    33,    34,    35,    36,    37,    38,    39,
      -1,    41,     4,     5,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    54,    -1,    -1,    -1,    -1,    21,
      -1,     4,     5,    -1,    26,    27,    28,    29,    30,    31,
      -1,    33,    34,    35,    36,    37,    38,    39,    40,    41,
       4,     5,    25,    26,    27,    28,    29,    30,    31,    -1,
      33,    34,    35,    36,    37,    38,    39,    21,    41,     4,
       5,    -1,    26,    27,    28,    29,    30,    31,    -1,    33,
      34,    35,    36,    37,    38,    39,    21,    41,     4,     5,
      -1,    26,    27,    28,    29,    30,    31,    -1,    33,    34,
      35,    36,    37,    38,    39,    21,    41,     4,     5,    -1,
      26,    27,    28,    29,    30,    31,    -1,    33,    34,    35,
      36,    37,    38,    39,    21,    41,     4,     5,    -1,    26,
      27,    28,    29,    30,    31,    -1,    33,    34,    35,    36,
      37,    38,    39,    -1,    41,     4,     5,    -1,    26,    27,
      28,    29,    30,    31,    -1,    33,    34,    35,    36,    37,
      38,    39,    40,    41,    -1,    -1,    -1,    26,    27,    28,
      29,    30,    31,    20,    33,    34,    35,    36,    37,    38,
      39,    -1,    41,    -1,    31,    -1,    -1,    34,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     3,     6,     7,     8,     9,    10,    12,    13,    14,
      15,    16,    17,    18,    19,    22,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    54,    57,    61,    62,
      63,    70,    20,    63,    20,    31,    34,    51,    52,    53,
      64,    65,    68,    70,    71,    20,    20,    51,    52,    54,
      55,    20,    51,    54,    71,    70,    70,    20,    20,    20,
      62,    41,     0,    62,    51,    64,    64,    20,    24,    41,
       4,     5,    26,    27,    28,    29,    30,    31,    33,    34,
      35,    36,    37,    38,    39,    41,    69,    64,    64,    51,
      55,    55,    63,    51,    71,    54,    54,    51,    51,    51,
      51,    64,    23,    64,    20,    24,    41,    54,    21,    21,
      64,    66,    67,    64,    64,    64,    64,    64,    21,    21,
      63,    63,    11,    21,    21,    20,    20,    21,    21,    21,
      54,    58,    59,    60,    70,    63,    71,    64,    63,    21,
      40,    21,    25,    63,    61,    11,    11,    54,    54,    54,
      63,    63,    54,    54,    21,    40,    51,    25,    25,    54,
      64,    67,    41,    54,    54,    21,    21,    61,    59,    24,
      54,    64,    25
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    56,    57,    58,    58,    59,    59,    60,    60,    61,
      62,    62,    63,    63,    63,    63,    63,    63,    63,    63,
      63,    63,    63,    63,    63,    63,    63,    63,    63,    63,
      63,    63,    63,    63,    63,    63,    63,    63,    63,    63,
      63,    64,    64,    64,    64,    64,    64,    64,    64,    64,
      65,    65,    65,    65,    65,    66,    66,    67,    67,    68,
      68,    69,    69,    69,    69,    69,    69,    69,    69,    69,
      69,    69,    69,    69,    69,    70,    70,    70,    70,    70,
      70,    70,    70,    70,    71,    71
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     0,     1,     1,     3,     2,     4,     3,
       1,     2,     0,     1,     5,     2,     5,     2,     5,     6,
       6,     5,     2,     1,     2,     3,     3,     5,     5,     5,
       5,     6,     6,     4,     4,     3,     5,     5,     6,     6,
       1,     1,     1,     2,     3,     3,     3,     4,     3,     6,
       1,     1,     3,     4,     4,     0,     1,     1,     3,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 3:
#line 99 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized empty parameters\n";}
#line 1452 "language.tab.c" /* yacc.c:1646  */
    break;

  case 4:
#line 100 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized ParamDeclListTail\n";}
#line 1458 "language.tab.c" /* yacc.c:1646  */
    break;

  case 5:
#line 103 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized a ParamDecl\n";}
#line 1464 "language.tab.c" /* yacc.c:1646  */
    break;

  case 6:
#line 104 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized a ParamList\n";}
#line 1470 "language.tab.c" /* yacc.c:1646  */
    break;

  case 7:
#line 107 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized ParamDecl\n";}
#line 1476 "language.tab.c" /* yacc.c:1646  */
    break;

  case 8:
#line 108 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Brackets ParamDecl";}
#line 1482 "language.tab.c" /* yacc.c:1646  */
    break;

  case 9:
#line 112 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized the end a Block\n";}
#line 1488 "language.tab.c" /* yacc.c:1646  */
    break;

  case 10:
#line 115 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized a statement\n";}
#line 1494 "language.tab.c" /* yacc.c:1646  */
    break;

  case 11:
#line 116 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized a statement recursion\n";}
#line 1500 "language.tab.c" /* yacc.c:1646  */
    break;

  case 13:
#line 120 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized a semi-colon ending statement\n";}
#line 1506 "language.tab.c" /* yacc.c:1646  */
    break;

  case 14:
#line 121 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized If statement\n";}
#line 1512 "language.tab.c" /* yacc.c:1646  */
    break;

  case 15:
#line 122 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Then statement\n";}
#line 1518 "language.tab.c" /* yacc.c:1646  */
    break;

  case 16:
#line 123 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Elif statment\n";}
#line 1524 "language.tab.c" /* yacc.c:1646  */
    break;

  case 17:
#line 124 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Else statement\n";}
#line 1530 "language.tab.c" /* yacc.c:1646  */
    break;

  case 18:
#line 125 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Switch statement\n";}
#line 1536 "language.tab.c" /* yacc.c:1646  */
    break;

  case 19:
#line 126 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Case statement\n";}
#line 1542 "language.tab.c" /* yacc.c:1646  */
    break;

  case 20:
#line 127 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized a Case statement\n";}
#line 1548 "language.tab.c" /* yacc.c:1646  */
    break;

  case 21:
#line 128 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Default statment\n";}
#line 1554 "language.tab.c" /* yacc.c:1646  */
    break;

  case 22:
#line 129 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Default statement\n";}
#line 1560 "language.tab.c" /* yacc.c:1646  */
    break;

  case 23:
#line 130 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognize Continue statement\n";}
#line 1566 "language.tab.c" /* yacc.c:1646  */
    break;

  case 24:
#line 131 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized a return statement\n";}
#line 1572 "language.tab.c" /* yacc.c:1646  */
    break;

  case 25:
#line 132 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Return statement\n";}
#line 1578 "language.tab.c" /* yacc.c:1646  */
    break;

  case 26:
#line 133 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Return statement\n";}
#line 1584 "language.tab.c" /* yacc.c:1646  */
    break;

  case 27:
#line 134 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Return statement\n";}
#line 1590 "language.tab.c" /* yacc.c:1646  */
    break;

  case 28:
#line 135 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Return statement\n";}
#line 1596 "language.tab.c" /* yacc.c:1646  */
    break;

  case 29:
#line 136 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Write statement\n";}
#line 1602 "language.tab.c" /* yacc.c:1646  */
    break;

  case 30:
#line 137 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized WriteLn statement\n";}
#line 1608 "language.tab.c" /* yacc.c:1646  */
    break;

  case 31:
#line 138 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Public statement\n";}
#line 1614 "language.tab.c" /* yacc.c:1646  */
    break;

  case 32:
#line 139 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Private statement\n";}
#line 1620 "language.tab.c" /* yacc.c:1646  */
    break;

  case 33:
#line 140 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized While statment\n";}
#line 1626 "language.tab.c" /* yacc.c:1646  */
    break;

  case 34:
#line 141 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized an re-assignment statement\n";}
#line 1632 "language.tab.c" /* yacc.c:1646  */
    break;

  case 35:
#line 142 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized an identifier\n";}
#line 1638 "language.tab.c" /* yacc.c:1646  */
    break;

  case 36:
#line 143 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized an assignment statement\n";}
#line 1644 "language.tab.c" /* yacc.c:1646  */
    break;

  case 37:
#line 144 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized an array declaration\n";}
#line 1650 "language.tab.c" /* yacc.c:1646  */
    break;

  case 38:
#line 145 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized a multi-dimensional array declaration\n";}
#line 1656 "language.tab.c" /* yacc.c:1646  */
    break;

  case 39:
#line 146 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognizes a declaration\n";}
#line 1662 "language.tab.c" /* yacc.c:1646  */
    break;

  case 41:
#line 150 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized a Primary Expression\n";}
#line 1668 "language.tab.c" /* yacc.c:1646  */
    break;

  case 43:
#line 152 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Unary expression\n";}
#line 1674 "language.tab.c" /* yacc.c:1646  */
    break;

  case 44:
#line 153 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Binary expression\n";}
#line 1680 "language.tab.c" /* yacc.c:1646  */
    break;

  case 45:
#line 154 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized And keyword\n";}
#line 1686 "language.tab.c" /* yacc.c:1646  */
    break;

  case 46:
#line 155 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Or keyword";}
#line 1692 "language.tab.c" /* yacc.c:1646  */
    break;

  case 47:
#line 156 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized an Expression list\n";}
#line 1698 "language.tab.c" /* yacc.c:1646  */
    break;

  case 48:
#line 157 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Declaration Expression\n";}
#line 1704 "language.tab.c" /* yacc.c:1646  */
    break;

  case 49:
#line 158 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Bracketed Expression\n";}
#line 1710 "language.tab.c" /* yacc.c:1646  */
    break;

  case 50:
#line 161 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized an Identifier\n";}
#line 1716 "language.tab.c" /* yacc.c:1646  */
    break;

  case 51:
#line 162 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized a type\n";}
#line 1722 "language.tab.c" /* yacc.c:1646  */
    break;

  case 52:
#line 163 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Parenthesis Expression\n";}
#line 1728 "language.tab.c" /* yacc.c:1646  */
    break;

  case 53:
#line 164 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized a Parenthesis Expression\n";}
#line 1734 "language.tab.c" /* yacc.c:1646  */
    break;

  case 54:
#line 165 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized a Square Bracket expression\n";}
#line 1740 "language.tab.c" /* yacc.c:1646  */
    break;

  case 55:
#line 168 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Empty ExprList\n";}
#line 1746 "language.tab.c" /* yacc.c:1646  */
    break;

  case 56:
#line 169 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized ExprListTail\n";}
#line 1752 "language.tab.c" /* yacc.c:1646  */
    break;

  case 57:
#line 172 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Expression\n";}
#line 1758 "language.tab.c" /* yacc.c:1646  */
    break;

  case 58:
#line 173 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Expression Recursion\n";}
#line 1764 "language.tab.c" /* yacc.c:1646  */
    break;

  case 59:
#line 176 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized subtraction operator\n";}
#line 1770 "language.tab.c" /* yacc.c:1646  */
    break;

  case 60:
#line 177 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized non-equivalency comparison\n";}
#line 1776 "language.tab.c" /* yacc.c:1646  */
    break;

  case 61:
#line 180 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Addition operator\n";}
#line 1782 "language.tab.c" /* yacc.c:1646  */
    break;

  case 62:
#line 181 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Subtraction operator\n";}
#line 1788 "language.tab.c" /* yacc.c:1646  */
    break;

  case 63:
#line 182 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Division operator \n";}
#line 1794 "language.tab.c" /* yacc.c:1646  */
    break;

  case 64:
#line 183 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Multiplication operator\n";}
#line 1800 "language.tab.c" /* yacc.c:1646  */
    break;

  case 65:
#line 184 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Modular divison operator\n";}
#line 1806 "language.tab.c" /* yacc.c:1646  */
    break;

  case 66:
#line 185 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Dot operator\n";}
#line 1812 "language.tab.c" /* yacc.c:1646  */
    break;

  case 67:
#line 186 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Exponent operator\n";}
#line 1818 "language.tab.c" /* yacc.c:1646  */
    break;

  case 68:
#line 187 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Assignment operator\n";}
#line 1824 "language.tab.c" /* yacc.c:1646  */
    break;

  case 69:
#line 188 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Equivalency evaluation\n";}
#line 1830 "language.tab.c" /* yacc.c:1646  */
    break;

  case 70:
#line 189 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Less Than evaluation\n";}
#line 1836 "language.tab.c" /* yacc.c:1646  */
    break;

  case 71:
#line 190 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Greater Than evaluation\n";}
#line 1842 "language.tab.c" /* yacc.c:1646  */
    break;

  case 72:
#line 191 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Less Than or Equal To evaluation\n";}
#line 1848 "language.tab.c" /* yacc.c:1646  */
    break;

  case 73:
#line 192 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Greater Than or Equal to evaluation\n";}
#line 1854 "language.tab.c" /* yacc.c:1646  */
    break;

  case 74:
#line 193 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Not Equivalent evaluation\n";}
#line 1860 "language.tab.c" /* yacc.c:1646  */
    break;

  case 75:
#line 196 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Int type\n";}
#line 1866 "language.tab.c" /* yacc.c:1646  */
    break;

  case 76:
#line 197 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized String type\n";}
#line 1872 "language.tab.c" /* yacc.c:1646  */
    break;

  case 77:
#line 198 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Double type\n";}
#line 1878 "language.tab.c" /* yacc.c:1646  */
    break;

  case 78:
#line 199 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Char type\n";}
#line 1884 "language.tab.c" /* yacc.c:1646  */
    break;

  case 79:
#line 200 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Float type\n";}
#line 1890 "language.tab.c" /* yacc.c:1646  */
    break;

  case 80:
#line 201 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Boolean type\n";}
#line 1896 "language.tab.c" /* yacc.c:1646  */
    break;

  case 81:
#line 202 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized LongInt type\n";}
#line 1902 "language.tab.c" /* yacc.c:1646  */
    break;

  case 82:
#line 203 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Var type\n";}
#line 1908 "language.tab.c" /* yacc.c:1646  */
    break;

  case 83:
#line 204 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized Void type\n";}
#line 1914 "language.tab.c" /* yacc.c:1646  */
    break;

  case 84:
#line 207 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized a number\n";}
#line 1920 "language.tab.c" /* yacc.c:1646  */
    break;

  case 85:
#line 208 "language.y" /* yacc.c:1646  */
    {std::cout<<"Recognized a double\n";}
#line 1926 "language.tab.c" /* yacc.c:1646  */
    break;


#line 1930 "language.tab.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 211 "language.y" /* yacc.c:1906  */


/* C++ code */

int main (int argc, char** argv)
{
	if (argc == 1)
	{
		std::cout<<"no source\n";
		return 0;
	}
	yyin = fopen(argv[1], "r");
	if(!yyin)
	{
		std::cout<<"could not open file\n";
		perror(argv[1]);
		return(1);
	}
	yyparse();
	return 0;
}

void yyerror (std::string error) 
{
	std::cout<< "Error: " << error << "\n";
}
