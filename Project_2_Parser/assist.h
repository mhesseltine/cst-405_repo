#ifndef __ASSIST_H__
#define __ASSIT_H__

#include "language.tab.h"

typedef struct t_generateTree
{
	TerminalType type;
	int value;
	struct t_generateTree *left;
	struct t_generateTree *right;
} generateTree;

generateTree *createOperation(yytokentype term, generateTree *left, generateTree *right);
generateTree *createIfOperation(yytokentype term, generateTree *left, generateTree *right);

void deleteExpression(generateTree *d);

#endif
